const Lightbox = require( './lightbox.js' );
const Nav = require( './nav.js' );
const Search = require( './search.js' );

// Reactive store singleton; this can be shared between different
// features/instances if necessary
const store = PetiteVue.reactive( {
    currentImage: null
} );

// Set up the lightbox feature
PetiteVue.createApp( { 
    store,
    Lightbox,
    $delimiters: ['${', '}'] 
} ).mount( '#incunabula-lightbox' );

// // Set up the search feature
// PetiteVue.createApp( {
//     Search,
//     $delimiters: ['${', '}'] 
// } ).mount( '#incunabula-search' )

// Set up the nav feature
PetiteVue.createApp( { 
    Nav,
    $delimiters: ['${', '}'] 
} ).mount( '#incunabula-toolbar' );

// Set up event listeners on the server-provided page content
document.querySelectorAll( 'a.image' )
    .forEach( ( image ) => {
        image.addEventListener( 'click', ( e ) => {
            e.preventDefault();
            store.currentImage = mw.util.parseImageUrl( 
                e.currentTarget.href 
            ).name;
        } ) ;
} );