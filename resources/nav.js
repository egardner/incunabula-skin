// const SEARCH_URL_BASE = 'https://en.wikipedia.org/w/rest.php/v1/search/title';
const SEARCH_URL_BASE = 'http://localhost:8080/w/api.php';
const SEARCH_RESULT_LIMIT = 10;

module.exports = function Nav() {
	return {
		active: null,  // the currently-active menu
		menus: [],     // IDs of all menus in the nav
		searchQuery: '',
		searchResults: [],
		searchExpanded: false,

		// Keep track of the currently expanded menu
		setActive( e ) {
			if ( e.target.open ) {
				this.active = e.target.id;
			} else {
				this.active = null;
			}
		},

		// If the active menu changes, ensure all other menus are closed
		closeOnChange( $el ) {
			if ( this.active && this.active !== $el.id ) {
				$el.open = false;
			}
		},

		onMounted( $el ) {
			$el.querySelectorAll( 'details' ).forEach( ( child ) => {
				this.menus.push( child.id );
			} );
		},

		fetchResults() {
			if ( !this.searchQuery ) {
				this.searchResults = [];
				return;
			}

			const searchUrl = `
				${SEARCH_URL_BASE}
				?action=query
				&format=json
				&list=search
				&srsearch=${encodeURIComponent( this.searchQuery )}
				&srlimit=${SEARCH_RESULT_LIMIT}
			`;

			function adaptApiResponse( results ) {
				return results.map( ( { title, pageid, snippet } ) => ( {
					label: title,
					value: pageid,
					url: `http://localhost:8080/wiki/${encodeURIComponent(title)}`
				} ) );
			}

			fetch( searchUrl ).then( ( res ) => {
				return res.json()
			} ).then( ( data ) => {
				console.log( data );
				if ( data.query && data.query.search ) {
					this.searchResults = adaptApiResponse( data.query.search );
				} else {
					this.searchResults = [];
				}
			});
		},

		fetchResultsDebounced() {
			OO.ui.throttle( this.fetchResults() );
		}
	};
};