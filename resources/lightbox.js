const BASE_URL = '/w/api.php'
const REQ_PARAMS = {
    action: 'query',
    format: 'json',
    prop: 'imageinfo',
    continue: '||',
    iiprop: 'url',
    iiurlwidth: '800'
};

/**
 * In petite-vue, a "component" is simply a function. This allows for
 * private state and re-usable logic. The "component" can accept arguments
 * if it needs to be aware of any outer state.
 * 
 * @param {Object} props
 * @param {string|null} props.currentImage
 */
module.exports = function Lightbox( props ) {
    return {
        // Local state
        client: null,
        ready: false,
        sourceUrl: null,
        descriptionUrl: null,

        // Basic computed properties via getters
        get isVisible() {
            return !!props.currentImage;
        },

        get title() {
            if ( props.currentImage ) {
                return props.currentImage
            } else {
                return null;
            }
        },

        // methods
        getImageInfo() {
            if ( !props.currentImage ) {
                return;
            }
            this.ready = false;
            this.client.get( {
                ...REQ_PARAMS,
                titles: props.currentImage
            } ).done( ( data ) => {
                const imageData = Object.values( data.query.pages )[ 0 ].imageinfo[ 0 ];
                this.sourceUrl = imageData.thumburl;
                this.descriptionUrl = imageData.descriptionurl;
                this.ready = true;
            } );
        },

        closeLightbox() {
            props.currentImage = null;
        },

        // Method to call on mounted hook (see template)
        mounted() {
            this.client = new mw.Api();
        }
    }
};