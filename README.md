> :warning: This project is for demonstration purposes only and is not intended
> for production use!
## Incunabula Skin

Incunabula is a prototype MediaWiki skin intended to demonstrate a modern
approach to progressive enhancement. It is designed with the following
principles in mind:

* Responsive design: The skin is intended to serve both desktop and mobile users
* Progressive enhancement: Users with JS disabled should retain a first-class
  experience

This skin does not support legacy browsers like Internet Explorer; support for
modern HTML (`<details>`), CSS (flexbox), and JS (ES6) is required.

### Tech choices

The tech used in this skin is intentionally simple, intended to fit in with
MediaWiki's existing tech stack. Incunabula uses Mustache templates on the
server to build out a basic UI. This is enhanced in the browser when JS is
available with [petite-vue](https://github.com/vuejs/petite-vue), an alternative
distribution of the [Vue.js](https://vuejs.org/) framework optimized for
progressive enhancement use-cases. Petite-vue includes Vue's reactivity system
but uses the real DOM instead of a virtual one; existing HTML elements are
re-used instead. Most importantly, the total size of this library when minified
and GZipped is ~7kb.

There is no JS build step, no NPM or `node_modules`, no JS toolchain beyond MW's
ResourceLoader. Mustache templates include Vue syntax which is used by Petite-Vue
as soon as JS initializes to create a reactive user interface; this part of the
markup is simply ignored if JS is not enabled.

### Limitations of this approach

* Mustache is a very limited templating language. This makes "componentization"
  pretty difficult; it would be easier to split up templates into smaller pieces
  closer to their JS equivalents if we had access to some thing like Twig (which
  allows passing parameters to partials).
* ResourceLoader strips out `<template>` tags from any templates it loads; this
  limits your options when trying to define the template for a JS-only UI feature
  that should not show up without some kind of user interaction.

### Features

* Expandible page nav menus without JS (using `<details>` and `<summary>` elements)
* Lightbox feature (JS only)
* "Typeahead-lite" search feature (enhanced version requires JS, simple version does not)


