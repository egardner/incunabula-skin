<?php

class Incunabula extends SkinMustache {
	/**
	 * Extends the getTemplateData function to add a template key 'html-myskin-hello-world'
	 * which can be rendered in skin.mustache using {{{html-myskin-hello-world}}}
	 * @return data
	 */
	public function getTemplateData() {
		// Add Codex styles in PHP so that they are available without JS
		$this->getOutput()->addModuleStyles( [ 'codex-styles' ] );

		// Icons can be fetched on the server using the following logic;
		// to display a given icon in Mustache, do this:
		// <svg>{{{cdx-icon-search}}}</svg>
		$icons = \ResourceLoaderCodexModule::getIcons(
			\ResourceLoaderContext::newDummyContext(),
			$this->getConfig(),
			[
				'cdxIconSearch',
				'cdxIconMenu',
				'cdxIconClose',
				'cdxIconBell',
				'cdxIconUserAvatar',
				'cdxIconGlobe',
				'cdxIconSettings',
				'cdxIconImageLayoutFrameless'
			]
		);

		$data = parent::getTemplateData();

		$data[ 'cdx-icon-search' ] = $icons[ 'cdxIconSearch' ];
		$data[ 'cdx-icon-close' ] = $icons[ 'cdxIconClose' ];
		$data[ 'cdx-icon-thumbnail' ] = $icons[ 'cdxIconImageLayoutFrameless' ];

		// Set up custom icons for menus
		$data[ 'data-portlets' ][ 'data-notifications' ][ 'incunabula-icon' ] = $icons[ 'cdxIconBell' ];
		$data[ 'data-portlets' ][ 'data-user-menu' ][ 'incunabula-icon' ] = $icons[ 'cdxIconUserAvatar' ];
		$data[ 'data-portlets-sidebar' ][ 'data-portlets-first' ][ 'incunabula-icon' ] = $icons[ 'cdxIconGlobe' ];
		$data[ 'data-portlets-sidebar' ][ 'array-portlets-rest' ][ 0 ][ 'incunabula-icon' ] = $icons[ 'cdxIconSettings' ];

		return $data;
	}
}
